#include <iostream> 
#include "GradientDescent.h"
#include "NewtonMethod.h"
#include "DiscreteEnergy.h"

using Eigen::VectorXd;

int main(int argc, char *argv[])
{
	// Get discrete energy. 
    DiscreteEnergy energy;

	// Get Armijo rule.
	ArmijoRule armijo(energy);

	// Get gradient descent.
	GradientDescent solver(energy, armijo);
	solver.setLogLevel(1);

	// Get newton solver.
	NewtonMethod solver2(energy, armijo);
	solver2.setLogLevel(1); 

	// Get initial guess.
	VectorXd solution = VectorXd::Constant(2, 20);
	VectorXd solution2 = VectorXd::Constant(2, 20); 	

	// Minimize energy.
	double error = solver.solve(solution);	
	double error2 = solver2.solve(solution2);
	
	// Output solution (should be (1,1) for the Rosenbrock function).
	std::cout << "\nSolution (GradientDescent):\n" << solution << std::endl;
	std::cout << "Error: " << error << std::endl;

		// Output solution (should be (1,1) for the Rosenbrock function).
	std::cout << "\nSolution (NewtonMethod):\n" << solution2 << std::endl;
	std::cout << "Error: " << error2 << std::endl; 
	
    return 0;
}
