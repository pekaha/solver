/*! \file DiscreteEnergy.h
 * \author Pascal Huber <pascal@huber.kim>
 * \date May, 2017
 *
 * \brief Dummy energy class. 
 *
 */
#ifndef __DISCRETE_ENERGY_H
#define __DISCRETE_ENERGY_H

#include <Eigen/Dense>

using Eigen::VectorXd;
using Eigen::MatrixXd; 

// Dummy discrete energy (Rosenbrock function in two dimensions) 
class DiscreteEnergy {
public: 
	double computeDiscreteEnergy(const VectorXd& value) const {
		return (1.0-value(0))*(1.0-value(0))
			+ 100.0*((value(1)- value(0)*value(0))*(value(1)- value(0)*value(0))); 
	}
	
	VectorXd computeD1DiscreteEnergy(const VectorXd& value) const {
		VectorXd result(2);
		result(0) = -2.0*(1.0-value(0)) - 400.0*(value(1) - value(0)*value(0))*value(0);
		result(1) = 200.0*(value(1) - value(0)*value(0));
		return result; 
	}

	MatrixXd computeD2DiscreteEnergy(const VectorXd& value) const {
		MatrixXd result(2,2);
		result(0,0) = 2.0 - 400.0*value(1) + 1200.0*value(0)*value(0);
		result(1,0) = -400.0*value(0);
		result(0,1) = -400.0*value(0); 
		result(1,1) = 200.0;
		return result; 
	}
}; // end class DiscreteEnergy

#endif // __DISCRETE_ENERGY_H
