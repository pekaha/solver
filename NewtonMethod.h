/*! \file NewtonMethod.h
 * \author Pascal Huber <pascal@huber.kim>
 * \date May, 2017
 *
 * \brief Simple (globalized) Newton method with Armijo step width control.
 *
 */
#ifndef __NEWTON_METHOD_H
#define __NEWTON_METHOD_H

#include <cmath>
#include <Eigen/Dense>

#include "GradientDescent.h"
#include "DiscreteEnergy.h"

using Eigen::VectorXd;
using Eigen::MatrixXd;
using Eigen::ColPivHouseholderQR;

/*! Class implementing a simple (globalized) Newton method with Armijo step width control.
 * The method is a combination of Newton method and gradient descent.
 * In each iteration the solver tries to do a Newton step (using a generalized angle condition)
 * if this is not possible a gradient descent step is applied.
 * The solver stops if the energy gradient falls below a certain tolerance or if the maximum 
 * number of iteration steps is attained (in which case the solver most probably was not able 
 * to find a solution). 
 */
class NewtonMethod {
public:
	//! Constructor.
	NewtonMethod(DiscreteEnergy& energy,
				 ArmijoRule& armijo,
				 const unsigned max_num_iterations = 1e6,
				 const double tolerance = 1e-8,
				 const double alpha = 1e-6,
				 const double p = 2.1)
		: energy_(&energy), armijo_(&armijo),
		  max_num_iterations_(max_num_iterations),
		  tolerance_(tolerance),
		  log_level_(1),
		  alpha_(alpha),
		  p_(p) {}

	/*! Set parameters.
	 * The number of iterations specifies the maximum number of 
	 * solver iterations before the solver stops (if the stopping 
	 * criterion was not satisfied before). 
	 * The tolerance parameter specifies how small the energy  
	 * gradient must be before the solver stops. 
	 *
	 * \param max_num_iterations   maximum number of iterations
	 * \param tolerance            error tolerance
	 */
	void setParameters(const unsigned max_num_iterations = 1e6,
					   const double tolerance = 1e-8) {
		max_num_iterations_ = max_num_iterations;
		tolerance_ = tolerance;
	}

	//! Set log level (between 0 (no log) and 2 (log everything).
	void setLogLevel(const unsigned level) {
		log_level_ = level;
	}

	/*! Main method for solving the optimization problem.
	 *  The method gets as argument an initial guess for the
	 * solution. It stores the final result in the argument and
	 * returns the norm of the energy gradient at the final result.
	 *
	 * \param[in,out] value      initial guess and storage for final result
	 * \return norm of energy gradient evaluated at final result
	 */
	double solve(VectorXd& value) {
		// compute energy at current value
		double energy_value = energy_->computeDiscreteEnergy(value);
		// compute energy gradient at current value
		VectorXd energy_gradient = energy_->computeD1DiscreteEnergy(value);
		// initialize line search direction
		VectorXd line_direction = -energy_gradient;
		// initialize the number of iterations
		unsigned iteration = 0;
		// initialize step width
		double step_width = 1.0;
		// initialize current error
		double error = energy_gradient.norm();
		// repeat steps until error is small enough or maximum number of iterations is reached
		while (error > tolerance_
			   && iteration < max_num_iterations_) {
			// get energy hessian at current value
			MatrixXd energy_hessian = energy_->computeD2DiscreteEnergy(value);
			// initialize linear solver
			ColPivHouseholderQR<MatrixXd> linear_solver(energy_hessian);
			// check if hessian is invertible
			if (linear_solver.isInvertible()) {
				// compute line direction according to Newton 
				line_direction = linear_solver.solve(-energy_gradient);
				// check angle condition
				if (satisfiesAngleCondition(line_direction, energy_gradient) == false) {
					line_direction = -energy_gradient;
				}
			}
			// get armijo step width
			step_width = (*armijo_)(value, line_direction,
								   energy_gradient, energy_value);
			// update value
			value += step_width*line_direction;
			// update energy value and energy gradient
			energy_value = energy_->computeDiscreteEnergy(value);
			energy_gradient = energy_->computeD1DiscreteEnergy(value);
			// update error
			error = energy_gradient.norm();
			// update iteration
			iteration++;
			// log if requested
			if (log_level_ > 1) {
				std::cout << "iteration: \t" << iteration
						  << "\t\tenergy: \t" << energy_value
						  << "\t\terror: \t" << error << "\n";
			}
		}
		// log if requested
		if (log_level_ > 0) {
			std::cout << "\nFinal result: \n-------------\n"
					  << "#iterations: \t" << iteration
					  << "\t\tenergy: \t" << energy_value
					  << "\t\terror: \t" << error << std::endl;
		}
		// return error
		return error;
	}

protected:
	//! Check generalized angle condition.
	bool satisfiesAngleCondition(const VectorXd& line_direction,
								 const VectorXd& gradient) {
		return (gradient.dot(line_direction)
				<= -alpha_*std::pow(line_direction.norm(), p_)); 
	}
	
private:
	// Energy object.
	DiscreteEnergy* energy_;
	// Armijo functor.
	ArmijoRule* armijo_;
	// Maximum number of iterations.
	unsigned max_num_iterations_;
	// Tolerance.
	double tolerance_;
	// First parameter for angle condition;
	double alpha_;
	// Second parameter for angle condition;
	double p_; 
	// Switch for logging (between 0 (no log) and 2 (log everything).
	unsigned log_level_;
}; // end class NewtonMethod
	

#endif // __NEWTON_METHOD_H
