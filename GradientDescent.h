/*! \file gradientDescent.h
 * \author Pascal Huber <pascal@huber.kim>
 * \date May, 2017
 *
 * \brief Simple gradient descent with Armijo step width control. 
 *
 */
#ifndef __GRADIENT_DESCENT_H
#define __GRADIENT_DESCENT_H

#include <Eigen/Dense>

#include "DiscreteEnergy.h"

/*! Functor implementing the Armijo rule for step width control. 
 */
class ArmijoRule {
public:
	//! Constructor.
	ArmijoRule(DiscreteEnergy& energy,
			   const double beta = 0.5,
			   const double gamma = 1e-4)
		: energy_(&energy), beta_(beta), gamma_(gamma) {}

	//! Set parameters.
	void setParameters(const double beta = 0.5,
					   const double gamma = 1e-4) {
		beta_ = beta;
		gamma_ = gamma; 
	}

	//! Set energy.
	void setEnergy(DiscreteEnergy& energy) {
		energy_ = &energy; 
	}

	/*! Function call operator.
	 * 
	 * \param value            current value of control points d
	 * \param energy_gradient  value of energy gradient at d 
	 * \param energy_value     value of energy at d
	 * \return step width
	 */
	double operator()(const VectorXd& value,
					  const VectorXd& line_direction, 
					  const VectorXd& energy_gradient,
					  const double energy_value) {
		// initialize step width
		double width = 1.0/beta_;
		// compute iteration
		VectorXd temp_value = value + width*line_direction;
		// decrease step width until Armijo rule is satisfied
		while (energy_->computeDiscreteEnergy(temp_value) >
			   energy_value + width*gamma_*energy_gradient.dot(line_direction)) {
			// decrease step width
			width *= beta_;
			// update temp_value
			temp_value = value + width*line_direction; 
		}
		// return step width
		return width; 
	}
	
private:
	//! Energy.
	DiscreteEnergy* energy_; 	
	//! Parameter used for backward search. 
	double beta_;
	//! Parameter for comparison. 
	double gamma_;
}; // end class ArmijoRule


/*! Class implementing a simple gradient descent with Armijo step width control. 
 */
class GradientDescent {
public:
	//! Constructor.
	GradientDescent(DiscreteEnergy& energy,
					ArmijoRule& armijo,
					const unsigned max_num_iterations = 1e6,
					const double tolerance = 1e-8)
		: energy_(&energy), armijo_(&armijo),
		  max_num_iterations_(max_num_iterations),
		  tolerance_(tolerance),
		  log_level_(1) {}

	//! Set parameters.
	void setParameters(const unsigned max_num_iterations = 1e6,
					   const double tolerance = 1e-8) {
		max_num_iterations_ = max_num_iterations;
		tolerance_ = tolerance; 
	}

	//! Enable/disable log.
	void setLogLevel(const unsigned level) {
		log_level_ = level; 
	}

	//! Set energy.
	void setEnergy(DiscreteEnergy& energy) {
		energy_ = &energy;
	}

	/*! Main method for solving the optimization problem.
	 *  The method gets as argument an initial guess for the 
	 * solution. It stores the final result in the argument and 
	 * returns the norm of the energy gradient at the final result. 
	 * 
	 * \param[in,out] value      initial guess and storage for final result
	 * \return norm of energy gradient evaluated at final result
	 */ 
	double solve(VectorXd& value) {
		// compute energy at current value
		double energy_value = energy_->computeDiscreteEnergy(value); 
		// compute energy gradient at current value
		VectorXd energy_gradient = energy_->computeD1DiscreteEnergy(value);
		// initialize the number of iterations
		unsigned iteration = 0;
		// initialize step width
		double step_width = 1.0;
		// initialize current error
		double error = energy_gradient.norm(); 
		// repeat steps until error is small enough or maximum number of iterations is reached
		while (error > tolerance_
			   && iteration < max_num_iterations_) {
			// get armijo step width
			step_width = (*armijo_)(value, -energy_gradient,
									energy_gradient, energy_value);
			// update value
			value -= step_width * energy_gradient;
			// update energy value and energy gradient
			energy_value = energy_->computeDiscreteEnergy(value);
			energy_gradient = energy_->computeD1DiscreteEnergy(value);
			// update error
			error = energy_gradient.norm();
			// update iteration
			iteration++;
			// log if requested
			if (log_level_ > 1) {
				std::cout << "iteration: \t" << iteration
						  << "\t\tenergy: \t" << energy_value
						  << "\t\terror: \t" << error << "\n"; 
			}
		}
		// log if requested
		if (log_level_ > 0) {
			std::cout << "\nFinal result: \n-------------\n"
					  << "#iterations: \t" << iteration
					  << "\t\tenergy: \t" << energy_value
					  << "\t\terror: \t" << error << std::endl; 
		}
		// return error
		return error; 
	}
	
private:
	// Energy object. 
	DiscreteEnergy* energy_;
	// Armijo functor.
	ArmijoRule* armijo_;
	// Maximum number of iterations.
	unsigned max_num_iterations_;
	// Tolerance.
	double tolerance_;
	// Switch for logging (between 0 (no log) and 2 (log everything).
	unsigned log_level_;
}; // end class GradientDescent
				

#endif // __GRADIENT_DESCENT_H
