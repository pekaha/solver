# Quick notes on solvers


## Introduction

This directory contains (quite basic) implementation of 

-   a gradient descent solver and
-   a Newton iteration solver.

Both solvers try to find critical points of a class `DiscreteEnergy` which can be 
passed to the solver. Note that the solver rely on the `Eigen` library. 
The energy must implement the following methods: 

-   `DiscreteEnergy::computeDiscreteEnergy(VectorXd value)`;
-   `DiscreteEnergy::computeD1DiscreteEnergy(VectorXd value)`;
-   `DiscreteEnergy::computeD2DiscreteEnergy(VectorXd value)` (only Newton method).


## Step width control

Currently both solvers use only Armijo step with control which is implemented in
the class `ArmijoRule`. This step width control struct also must be passed to 
the solver's constructor. 

## Installation

The project can be installed using cmake. In the root directory create a new 
`build` directory

	mkdir build

and then invoke cmake in it

	cd build
	ccmake ..

After that the project is compiled using the `make` command. The main function can be 
run with 

	./main


## Usage

    // Construct energy. 
    DiscreteEnergy energy; 
    /*...*/ 
    // Initialize armijo step width control with default parameters. 
    ArmijoRule armijo(energy); 
    // Initialize Newton solver with default parameters. 
    NewtonMethod solver(energy, armijo);  
    // Set logging level for output to stdin during execution. 
    solver.setLogLevel(2); 
    // Initialize solution with an initial guess. 
    VectorXd solution = initial_guess; 
    // Minimize energy and store error in variable. 
    double error = solver.solve(solution); 
    // Print out solution. 
    std::cout << solution << std::endl; 


## References

-   <https://www.uni-due.de/~adf040p/skripte/NlOptSkript15.pdf> 
    (p. 23, pp. 28, pp. 44)
-   <http://www.mathematik.uni-stuttgart.de/~harrach/lehre/Optimierung.pdf>
    (pp. 18,  pp. 50)

